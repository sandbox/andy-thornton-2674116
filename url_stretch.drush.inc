<?php
/**
 * Drush commands working with URL Alias
 */


/**
 * Implements books_drush_help().
 */
function url_stretch_drush_help($command) {
  switch ($command) {
    case 'drush:url_stretch':
      return dt('Extend URL Alias');
    case 'meta:url_stretch:title':
      return dt("Red Hat Patches");
    case 'meta:url_stretch:summary':
      return dt("Modifications to drupal core to allow longer URL");
  }
}

/**
 * Implements hook_drush_command().
 */
function url_stretch_drush_command() {
  $items['url-rollback'] = array(
      'callback' => 'url_stretch_drush_rollback',
      'description' => "Reset to original url_alias table",
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'aliases' => array('urlback'),
  );
  $items['url-schema-display'] = array(
    'callback' => 'url_stretch_drush_schema_alias',
    'description' => "Show the schema for the URL alias table",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('urlschema'),
  );

  return $items;
}

/**
 * command: Rollback URL Aliases Database table
 */
function url_stretch_drush_revert_cpdrupal_727() {
    $tablename = "url_alias_" . date("Ymd_G_i_s");
  if (drush_confirm("Please confirm you want to roll back the url_alias table?")) {
      db_rename_table("url_alias", $tablename);
      db_rename_table("url_alias_backup", "url_alias");
      drush_print("url_alias rollback complete");
  } else {
    drush_print("url_alias rollback cancelled");
  }
}

/**
 * command: display schema for url_alias;
 */
function url_stretch_drush_schema_alias() {
    $schema = drupal_get_schema("url_alias");
    drush_print_r($schema);
}
