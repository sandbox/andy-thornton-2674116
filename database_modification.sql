SET global innodb_file_format="BARRACUDA";
SET global innodb_large_prefix=1;

 CREATE TABLE `url_alias_tmp` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'A unique path alias identifier.',
  `source` varchar(255) NOT NULL DEFAULT '' COMMENT 'The Drupal path this alias is for; e.g. node/12.',
  `alias` varchar(512) NOT NULL DEFAULT '' COMMENT 'The alias for this path; e.g. title-of-the-story.',
  `language` varchar(12) NOT NULL DEFAULT '' COMMENT 'The language this alias is for; if ’und’, the alias will be used for unknown languages. Each Drupal path can have an alias for each supported language.',
  PRIMARY KEY (`pid`),
  KEY `alias_language_pid` (`alias`,`language`,`pid`),
  KEY `source_language_pid` (`source`,`language`,`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=8205932 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='A list of URL aliases for Drupal paths; a user may visit...';

INSERT INTO url_alias_tmp SELECT * FROM url_alias;
RENAME TABLE url_alias to url_alias_backup;
RENAME TABLE url_alias_tmp to url_alias;

SET global innodb_file_format="Antelope";
SET global innodb_large_prefix=0;