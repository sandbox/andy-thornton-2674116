Extending the URL alias beyond 255 Characters.

Drupal stores path entries in the url_alias table, which has a field set to 255 characters. The field is also set as
a key and the default size for a key is 767 bytes in mysql and mariadb. To extend the field beyond that, you need to
modify both drupal core's schema and all forms which reference the path. You also need to adjust the database directly.

The module contains a schema change and the form_alters to adjust drupal. The database change is done in a separate
SQL file called database_modification.sql I have included a function in the module to make the database changes in
drupal. However, there is a caveat. You must save the row format as DYNAMIC and drupal stores it by default as COMPACT
This has the side affect of MySQL ignoring the filesystem setting on the table from BARACUDDA and uses ANTELOPE which
doesn't support the setting.

In D6, there was a way of profiding a mysel_suffix to the creation of the table, but it looks like it was dropped on
D7 to specifically define parameters without the ability to pass custom settings. I left it in the module in case anyone
has a recommendation past this so we can do the entire change within drupal.

I can't stress this enough, backup you're database before making this change. I have provided a rollback command to
reset the url_alias table back and make a backup of the new table so you can write you're own code to manage changes
between the time you implement the change to the time you rollback. All it does is rename the tables back and makes a
backup with timestamp of the Baracudda version of the table.

    "drush urlback"

This code is for reference only. If anyone is interested in collaborating to make this more production worthy, I am
happy to do so. Getting round the lack of the ability to use a mysql_suffix would make this process easier and not
require a separate sql file.

Here's the logic behind it:

MySQL keys are limited to 767 bytes when two conditions exist. The file system for the table is set to Anterlope and
the innodb_large_prefix is not set or set to Off. 255 chars on a URL = 764 bytes. So if we want to extend it to 512,
MySQL needs to support larger keysizes.

To do this, we enable the file system to use BARRACUDA and enable the innodb_large_prefix variable to On. Then we
create a copy of the URL_ALIAS table, makind sure to set the ROW_FORMAT=DYNAMIC. If it's left as the default, MySQL
will regard the table as formatted as ANTELOPE and not allow the key change.

I leave the original table intact and name it as url_alias_backup and the drush command renames the new table with a
timestamp and renames the backup as url_alias. There may be changes done to nodes once the change is made, so having
the other table as a reference means you can compare the two and write a query to sync the data up if need be. Remember
that copying a field entry greater than 255 chars into the original table, will throw an error.

I have been a bit excessive on the error trapping, more so I can see where the errors occur and as most db operations
will often throw a 500 error if something goes wrong.

Thanks to Red Hat for supporting the development, I will do more testing and extend the module as I work on it further.
Any collaboration is always welcome.

Hope this code is helpful, I am always happy to answer any questions,

Andy Thornton
andy.thornton@redhat.com / andy.thornton@gmail.com